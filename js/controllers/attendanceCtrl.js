/**
 * Created by chirag on 03/05/16.
 */

app.controller("attendanceCtrl",['authenticationSvc','$scope','$location','$http','mainSvc','$rootScope','$filter','attendanceSvc',
    function(authenticationSvc,$scope,$location,$http,mainSvc,$rootScope,$filter,attendanceSvc){
        $scope.today = new Date()
        $scope.sdate = new Date();
        $scope.edate = new Date();
        $scope.dw_date = new Date();
        $scope.edate.setHours(0, 0, 0, 0);
        $scope.sdate = new Date($scope.edate);
        $scope.sdate.setDate($scope.edate.getDate() - 14);
        $scope.sdate.setHours(0, 0, 0, 0);
        $scope.summary_data = {};
        $scope.daywise_data = {};
        $scope.errormsg = '';
        $scope.is_checked_in = attendanceSvc.is_checked_in;
        $scope.need_checkout = attendanceSvc.need_checkout;

        $scope.checkin_attendance = '';
        $scope.checkin_reason = '';
        $scope.checkin_remarks = '';
        $scope.checkout_remarks = '';

        $scope.$watch(function() { return attendanceSvc.is_checked_in },
            function(newValue, oldValue) {
                $scope.is_checked_in = attendanceSvc.is_checked_in;
            }
        );

        $scope.$watch(function() { return attendanceSvc.need_checkout },
            function(newValue, oldValue) {
                $scope.need_checkout = attendanceSvc.need_checkout;
            }
        );

        $scope.mark_attendance = function(){
            //2016-06-02 10:52:31
            var date = $filter('date')(new Date(), 'yyyy-MM-dd hh:mm:ss')
            console.log($scope.checkin_reason);
            console.log($scope.checkin_remarks);

            var rel_path = '';
            for (var i = 0; i < $scope.attendance_urls.length; i++){
                if($scope.attendance_urls[i].component == 'attendance_mark'){
                    rel_path = $scope.attendance_urls[i].component_url.submit_attendance
                }
            }

            $http.get(mainSvc.api_rooturl + "/api/"+rel_path+"username=" + authenticationSvc.getUserInfo().username + "&for_date=" + date + "&attendance=" + $scope.checkin_attendance + "&checkin_reason=" + $scope.checkin_remarks + "&checkin_remarks=" + $scope.checkin_reason )
                .then(function (result) {
                    if(result.data.isError){
                        $scope.errormsg = result.data.msg;
                    }
                    else {
                        $scope.load_data();
                        $scope.load_attendance();
                        $location.path('/view_attendance')
                    }
                }, function(error) {
                    $scope.errormsg = error;
                });
        }

        $scope.load_attendance = function(){
            attendanceSvc.load_attendance()
        }

        $scope.mark_checkout = function(){
            //var date = $filter('date')($scope.today, 'yyyy-MM-dd')
            var date = $filter('date')(new Date(), 'yyyy-MM-dd hh:mm:ss')
            $http.get(mainSvc.api_rooturl + "/api/attendance/checkout/?username=" + authenticationSvc.getUserInfo().username + "&for_date=" + date + "&checkout_remarks=" + $scope.checkout_remarks )
                .then(function (result) {
                    if(result.data.isError){
                        $scope.errormsg = result.data.msg;
                    }
                    else {
                        $scope.load_data();
                        $scope.load_attendance();
                        $location.path('/view_attendance')
                    }
                }, function(error) {
                    $scope.errormsg = error;
                });
        }

        $scope.uploadcheckinpic = function(files){
            var fd = new FormData();
            fd.append("file", files[0]);
            var date = $filter('date')(new Date(), 'yyyy-MM-dd')
            $http.post(mainSvc.api_rooturl + '/api/docs/upload/?username='+authenticationSvc.getUserInfo().username+'&date='+date+'&doctype=checkin',fd,{
                headers: {'Content-Type': undefined },
                transformRequest: angular.identity
            }).then(function (result) {
                if(result.data.isError){
                    $scope.errormsg = result.data.msg;
                }
                else {
                    //$scope.get_profile();
                }

            }, function(error) {
                $scope.errormsg = error;
            });
        }

        $scope.uploadcheckoutpic = function(files){
            var fd = new FormData();
            fd.append("file", files[0]);
            var date = $filter('date')(new Date(), 'yyyy-MM-dd')
            $http.post(mainSvc.api_rooturl + '/api/docs/upload/?username='+authenticationSvc.getUserInfo().username+'&date='+date+'&doctype=checkout',fd,{
                headers: {'Content-Type': undefined },
                transformRequest: angular.identity
            }).then(function (result) {
                if(result.data.isError){
                    $scope.errormsg = result.data.msg;
                }
                else {
                    //$scope.get_profile();
                }

            }, function(error) {
                $scope.errormsg = error;
            });
        }

        $scope.load_data = function(){
            $scope.errormsg = '';
            var rel_path = '';
            for (var i = 0; i < $scope.attendance_urls.length; i++){
                if($scope.attendance_urls[i].component == 'view_attendance'){
                    rel_path = $scope.attendance_urls[i].component_url.view_attendance
                }
            }
            var sdate = $filter('date')($scope.sdate, 'yyyy-MM-dd')
            var edate = $filter('date')($scope.edate, 'yyyy-MM-dd')
            $http.get(mainSvc.api_rooturl + "/api/" +rel_path+ "username=" + authenticationSvc.getUserInfo().username + "&sdate=" +sdate+ "&edate=" + edate)
                .then(function (result) {
                    if(result.data.isError){
                        $scope.errormsg = result.data.msg;
                    }
                    else {
                        $scope.summary_data = result.data.data;
                    }
                }, function(error) {
                    $scope.errormsg = error;
                });
        }

        $scope.load_day_wisesummary = function(){
            $scope.errormsg = '';
            var rel_path = '';
            for (var i = 0; i < $scope.attendance_urls.length; i++){
                if($scope.attendance_urls[i].component == 'view_attendance'){
                    rel_path = $scope.attendance_urls[i].component_url.daywise_view_attendance
                }
            }
            //var sdate = $filter('date')($scope.sdate, 'yyyy-MM-dd')
            var edate = $filter('date')(new Date($scope.dw_date.getFullYear(), $scope.dw_date.getMonth(), 1), 'yyyy-MM-dd')
            $http.get(mainSvc.api_rooturl + "/api/"+rel_path+"username=" + authenticationSvc.getUserInfo().username + "&date=" +edate)
                .then(function (result) {
                    if(result.data.isError){
                        $scope.errormsg = result.data.msg;
                    }
                    else {
                        $scope.daywise_data = result.data.data;
                    }
                }, function(error) {
                    $scope.errormsg = error;
                });
        }

        $scope.changemonth = function(count){
            $scope.dw_date.setMonth($scope.dw_date.getMonth() + count);
            $scope.load_day_wisesummary();
        }

        $scope.load_attendance_config = function(){
            if(mainSvc.configs) {
                for (var i = 0; i < mainSvc.configs.length; i++) {
                    if (mainSvc.configs[i].module == "attendance") {
                        $scope.attendance_config = mainSvc.configs[i].configuration;
                        $scope.attendance_labels = mainSvc.configs[i].labels;
                        $scope.attendance_urls = mainSvc.configs[i].urls;
                    }
                }
                for (var i = 0; i < $scope.attendance_labels.length; i++){
                    if($scope.attendance_labels[i].component == 'view_attendance'){
                        $scope.view_page_title = $scope.attendance_labels[i].component_label
                        $scope.view_page_labels = $scope.attendance_labels[i].labels
                    }
                    if($scope.attendance_labels[i].component == 'day_wise_analysis'){
                        $scope.daywise_page_title = $scope.attendance_labels[i].component_label
                        $scope.daywise_page_labels = $scope.attendance_labels[i].labels
                    }
                    if($scope.attendance_labels[i].component == 'attendance_mark'){
                        $scope.markatt_page_title = $scope.attendance_labels[i].component_label
                        $scope.markatt_page_labels = $scope.attendance_labels[i].labels
                    }
                    if($scope.attendance_labels[i].component == 'checkout'){
                        $scope.checkout_page_title = $scope.attendance_labels[i].component_label
                        $scope.checkout_page_labels = $scope.attendance_labels[i].labels
                    }
                }
            }
            $scope.load_data();
        }

        $scope.load_attendance_config();
        $scope.load_attendance();
    }
])
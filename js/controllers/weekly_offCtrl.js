/**
 * Created by chirag on 03/05/16.
 */

app.controller("weekly_offCtrl",['authenticationSvc','$scope','$location','$http','mainSvc','$rootScope',
    function(authenticationSvc,$scope,$location,$http,mainSvc,$rootScope){
        $scope.woff = null
        $scope.errormsg = '';
        $scope.isedit = false;
        $scope.save_wo = function() {
            $scope.errormsg = '';
            $http({
                method: 'PUT',
                url: mainSvc.api_rooturl + "/api/weeklyoff/updateweeklyoff/?user_id="+authenticationSvc.getUserInfo().username,
                data:{weeklyoff: $scope.woff.week_day_no }
            }
            ).then(function (result) {
                //alert(result.data.msg);
                if(result.data.isError){
                    $scope.errormsg = result.data.msg;
                }
                else {
                    alert('Your Weekly Off has been updated!')
                    $scope.load_wo();
                    //$scope.isedit = false;
                    //$scope.new_feedback = {username:authenticationSvc.getUserInfo().username}
                }

            }, function(error) {
                $scope.errormsg = error;
            });
        }

        $scope.load_wo = function(){
            $http.get(mainSvc.api_rooturl + "/api/weeklyoff/getweeklyoff/?user_id="+authenticationSvc.getUserInfo().username)
                .then(function (result) {
                    //alert(result.data.msg);
                    if(result.data.isError){
                        $scope.errormsg = result.data.msg;
                    }
                    else {
                        $scope.woff = result.data.data
                        $scope.isedit = false;
                    }

                }, function(error) {
                    $scope.errormsg = error;
                });
        }

        $scope.load_wo();
    }
])
/**
 * Created by chirag on 03/05/16.
 */

app.controller("loginCtrl",['authenticationSvc','$scope','$location','$http','mainSvc','$rootScope','$window',
    function(authenticationSvc,$scope,$location,$http,mainSvc,$rootScope,$window){

        $scope.username = '';
        $scope.mypassword = '';
        $scope.errormsg = '';
        $scope.rememberMe = true;
        $scope.password_inputType = 'password';

        $scope.hideShowPassword = function(){
            if ($scope.show_password)
                $scope.password_inputType = 'text';
            else
                $scope.password_inputType = 'password';
            $scope.$apply();
        };

        if(authenticationSvc.getUserInfo()) {
            $scope.current_username = authenticationSvc.getUserInfo().username;
        }
        $scope.new_password = '';
        $scope.old_password = '' ;

        //if ($window.sessionStorage["userInfo"] && $window.sessionStorage["userInfo"]!=null
        //) {
        //    console.log($window.sessionStorage["userInfo"])
        //    $scope.username = JSON.parse($window.sessionStorage["userInfo"]).username;
        //    $scope.password = null;
        //}

        $scope.login = function() {
            $scope.errormsg = '';
            authenticationSvc.login($scope.username, $scope.mypassword, $scope.rememberMe, function (errormsg) {
                if (errormsg) {
                    $scope.errormsg = errormsg;
                }
                else
                {
                    $location.path("/");
                }
            })
        }

        $scope.change_password = function() {
            $scope.errormsg = '';
            if($scope.new_password != $scope.new_password_copy){
                $scope.errormsg = 'Passwords do not match';
            }
            else {
                $http.get(mainSvc.api_rooturl + "/api/login/change_password/?method=user_level&username=" + authenticationSvc.getUserInfo().username + "&password=" + $scope.old_password + "&newpassword=" + $scope.new_password)
                    .then(function (result) {
                        if (result.data.isError) {
                            $scope.errormsg = result.data.msg;
                        }
                        else {
                            alert('Your password has been changed successfully!');
                            $location.path("/");
                        }

                    }, function (error) {
                        $scope.errormsg = error;
                    });
            }
        }
    }
])
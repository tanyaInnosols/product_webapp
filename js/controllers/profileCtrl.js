/**
 * Created by chirag on 03/05/16.
 */

app.controller("profileCtrl",['authenticationSvc','$scope','$location','$http','mainSvc','$rootScope','$filter',
    function(authenticationSvc,$scope,$location,$http,mainSvc,$rootScope,$filter){
        $scope.profile = {}
        $scope.get_profile = function() {
            $scope.errormsg = '';
            var rel_path = '';
            for (var i = 0; i < $scope.profile_urls.length; i++){
                if($scope.profile_urls[i].component == 'profile'){
                    rel_path = $scope.profile_urls[i].component_url.get_profile
                }
            }
            $http.get(mainSvc.api_rooturl + "/api/"+ rel_path +"username=" + authenticationSvc.getUserInfo().username)
                .then(function (result) {
                    if(result.data.isError){
                        $scope.errormsg = result.data.msg;
                    }
                    else {
                        $scope.profile = result.data.data;
                    }
                }, function(error) {
                    $scope.errormsg = error;
                });
        }
        $scope.profile_config = {};
        $scope.load_profile_config = function(){
            if(mainSvc.configs) {
                for (var i = 0; i < mainSvc.configs.length; i++) {
                    if (mainSvc.configs[i].module == "profile") {
                        $scope.profile_config = mainSvc.configs[i].configuration;
                        $scope.profile_labels = mainSvc.configs[i].labels;
                        $scope.profile_urls = mainSvc.configs[i].urls;
                    }
                }
                for (var i = 0; i < $scope.profile_labels.length; i++){
                    if($scope.profile_labels[i].component == 'profile'){
                        $scope.page_title = $scope.profile_labels[i].component_label
                    }
                }
                $scope.get_profile();
            }
        }

        $scope.uploadprofilepic = function(files){
            var fd = new FormData();
            fd.append("file", files[0]);
            var date = $filter('date')(new Date(), 'yyyy-MM-dd')
            $http.post(mainSvc.api_rooturl + '/api/docs/upload/?username='+authenticationSvc.getUserInfo().username+'&date='+date+'&doctype=profile',fd,{
                headers: {'Content-Type': undefined },
                transformRequest: angular.identity
            }).then(function (result) {
                if(result.data.isError){
                    $scope.errormsg = result.data.msg;
                }
                else {
                    $scope.get_profile();
                }

            }, function(error) {
                $scope.errormsg = error;
            });
        }

        $scope.load_profile_config();


    }
])
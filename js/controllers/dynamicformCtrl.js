/**
 * Created by chirag on 03/05/16.
 */

app.controller("dynamicformCtrl",['authenticationSvc','$scope','$location','$http','mainSvc','$rootScope',
    function(authenticationSvc,$scope,$location,$http,mainSvc,$rootScope){
        $scope.d_form = {};
        $scope.next_form
        $scope.load_d_form = function(){
            $scope.mysearch = $location.search()
            if(mainSvc.forms) {
                for (var i = 0; i < mainSvc.forms.length; i++) {
                    if (mainSvc.forms[i].form_code) {
                        if (mainSvc.forms[i].form_code == $scope.mysearch.form_code) {
                            $scope.d_form = mainSvc.forms[i];
                        }
                    }
                    else{
                        if (mainSvc.forms[i].title == $scope.mysearch.title) {
                            $scope.d_form = mainSvc.forms[i];
                        }
                    }
                }
            }
        }

        $scope.dynamicdd = function(content_obj){
            if(content_obj.url_options) {
                if (content_obj.url_options.host) {
                    $http({
                        method: content_obj.url_options.method,
                        url: content_obj.url_options.host + content_obj.url_options.path + "?username=" + authenticationSvc.getUserInfo().username
                    }).then(function (result) {
                        if (result.isError) {
                            alert(result.msg)
                        }
                        else {
                            content_obj.options = [];
                            for (var i = 0; i < result.data.data.length; i++) {
                                content_obj.options.push(result.data.data[i].value)
                            }
                            if(result.data.data[0].url)
                                content_obj.url = result.data.data[0].url
                            if(result.data.data[0].child_label)
                                content_obj.child_label = result.data.data[0].child_label
                        }
                    })
                }
            }
        }


        $scope.dynamic_dd_fill = function(content_obj){
            $http({
                method: 'get',
                url: content_obj.url + '?value=' + content_obj.field_value + "&username=" + authenticationSvc.getUserInfo().username
            }).then(function (result) {
                if (result.isError) {
                    alert(result.msg)
                }
                else {
                    for (i = 0; i < $scope.d_form.content.length; i++) {
                        if ($scope.d_form.content[i].label == content_obj.child_label) {
                            $scope.d_form.content[i].options = []
                            for (j = 0; j < result.data.data.length; j++) {
                                $scope.d_form.content[i].options.push(result.data.data[j].value)
                            }
                            if(result.data.data[0]){
                                if(result.data.data[0].hasOwnProperty('url'))
                                    $scope.d_form.content[i].url = result.data.data[0].url
                                if(result.data.data[0].hasOwnProperty('child_label'))
                                    $scope.d_form.content[i].child_label = result.data.data[0].child_label
                            }
                        }
                    }
                }
            })
        }


        $scope.uploadfile = function(files,content_obj) {
            var fd = new FormData();
            fd.append("file", files[0]);
            console.log(content_obj)
            var date = $filter('date')(new Date(), 'yyyy-MM-dd')
            $http.post(mainSvc.api_rooturl + '/api/docs/upload/?username='+authenticationSvc.getUserInfo().username+'&date='+date+'&doctype=form&title='+$scope.d_form.title+'&form_code='+$scope.d_form.form_code+'&formfield='+content_obj.label+'&formfield_code=' + content_obj.label_key,fd,{
                headers: {'Content-Type': undefined },
                transformRequest: angular.identity
            }).then(function (result) {
                if(result.data.isError){
                    $scope.errormsg = result.data.msg;
                }
                else {
                    for (var i=0;i<$scope.d_form.content.length;i++){
                        if($scope.d_form.content[i].label_key) {
                            if ($scope.d_form.content[i].label_key == content_obj.label_key) {
                                $scope.d_form.content[i].field_value = result.data.data.doc.file.filename;
                            }
                        }
                        else{
                            if ($scope.d_form.content[i].label == content_obj.label) {
                                $scope.d_form.content[i].field_value = result.data.data.doc.file.filename;
                            }
                        }
                    }
                }

            }, function(error) {
                $scope.errormsg = error;
            });
        }

        $scope.post_form = function(){
            var data = {}
            data.title = $scope.d_form.title;
            data.form_code = $scope.d_form.form_code;
            for (var i=0;i<$scope.d_form.content.length;i++){
                if($scope.d_form.content[i].label_key) {
                    data[$scope.d_form.content[i].label_key] = $scope.d_form.content[i].field_value;
                }
                else{
                    data[$scope.d_form.content[i].label] = $scope.d_form.content[i].field_value;
                }
            }

            $http({
                    method: $scope.d_form.submit_url.method,
                    url: $scope.d_form.submit_url.host + $scope.d_form.submit_url.path + "?user=" + authenticationSvc.getUserInfo().username,
                    data: data
                })
                .then(function (result) {
                    if(result.data.isError){
                        $scope.errormsg = result.data.msg;
                    }
                    else {
                        alert(result.data.msg);
                        $location.path("/");
                    }

                }, function(error) {
                    $scope.errormsg = error;
                });
        }
        $scope.load_d_form();
    }
])
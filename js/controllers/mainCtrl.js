/**
 * Created by chirag on 03/05/16.
 */

app.controller("mainCtrl",['authenticationSvc','$scope','$location','$rootScope','mainSvc','attendanceSvc',
    function(authenticationSvc,$scope,$location,$rootScope,mainSvc,attendanceSvc){
        //alert($rootScope.forms)

        $scope.need_checkout = attendanceSvc.need_checkout;
        $scope.need_checkout_inconfig = false;
        $scope.$watch(function() { return attendanceSvc.need_checkout },
            function(newValue, oldValue) {
                $scope.need_checkout = attendanceSvc.need_checkout;
            }
        );
        attendanceSvc.load_attendance()


        $scope.logout = function() {
            //alert('hi');
            authenticationSvc.logout(function () {
                mainSvc.configs = [];
                $rootScope.configs = [];
                mainSvc.forms = [];
                $rootScope.forms = [];
                $location.path("/login");
            });
        }

        $scope.hasattendance = function(){
            if(mainSvc.configs) {
                for (var i = 0; i < mainSvc.configs.length; i++) {
                    if (mainSvc.configs[i].module == "attendance") {
                        $scope.need_checkout_inconfig = mainSvc.configs[i].configuration.need_checkout;
                        return true;
                    }
                }
            }
            return false;
        }

        $scope.hasprofile = function(){
            if(mainSvc.configs) {
                for (var i = 0; i < mainSvc.configs.length; i++) {
                    if (mainSvc.configs[i].module == "profile") {
                        return true;
                    }
                }
            }
            return false;
        }

        $scope.hasfeedback = function(){
            if(mainSvc.configs) {
                for (var i = 0; i < mainSvc.configs.length; i++) {
                    if (mainSvc.configs[i].module == "feedback") {
                        return true;
                    }
                }
            }
            return false;
        }

        $scope.hasattendance()
    }
])
/**
 * Created by chirag on 03/05/16.
 */

app.controller("feedbackCtrl",['authenticationSvc','$scope','$location','$http','mainSvc','$rootScope',
    function(authenticationSvc,$scope,$location,$http,mainSvc,$rootScope){
        $scope.new_feedback = {username:authenticationSvc.getUserInfo().username}
        $scope.errormsg = '';
            $scope.post_feedback = function() {
            $scope.errormsg = '';
            $scope.hit_url = true;

                if($scope.feedback_config.need_category) {
                    if ($scope.feedback_config.category_level == "warning" && (
                        $scope.new_feedback.category == null ||
                        $scope.new_feedback.category == undefined ||
                        $scope.new_feedback.category == '' )) {
                        if (window.confirm("Are you sure you don't want to select category?")) {
                            //$scope.hit_posturl
                        }
                        else {
                            $scope.hit_url = false;
                        }
                    }
                    else if ($scope.feedback_config.category_level == "mandatory" && (
                        $scope.new_feedback.category == null ||
                        $scope.new_feedback.category == undefined ||
                        $scope.new_feedback.category == '' )) {
                        $scope.errormsg = 'Please select category';
                        $scope.hit_url = false;
                    }
                }
                if($scope.feedback_config.need_subcategory ) {
                    if ($scope.feedback_config.subcategory_level == "warning" && (
                        $scope.new_feedback.subcategory == null ||
                        $scope.new_feedback.subcategory == undefined ||
                        $scope.new_feedback.subcategory == '' )) {
                        if (window.confirm("Are you sure you don't want to select subcategory?")) {
                            //$scope.hit_posturl
                        }
                        else {
                            $scope.hit_url = false;
                        }
                    }
                    else if ($scope.feedback_config.subcategory_level == "mandatory" && (
                        $scope.new_feedback.subcategory == null ||
                        $scope.new_feedback.subcategory == undefined ||
                        $scope.new_feedback.subcategory == '' )) {
                        $scope.errormsg = 'Please select subcategory';
                    }
                }

                if($scope.hit_url){
                    $scope.hit_posturl()
                }
        }

        $scope.hit_posturl = function(){
            var rel_path = '';
            for (var i = 0; i < $scope.feedback_urls.length; i++){
                if($scope.feedback_urls[i].component == 'feedback'){
                    rel_path = $scope.feedback_urls[i].component_url.submit_feedback
                }
            }
            $http.post(mainSvc.api_rooturl + "/api/" + rel_path,$scope.new_feedback)
                .then(function (result) {

                    if(result.data.isError){
                        $scope.errormsg = result.data.msg;
                    }
                    else {
                        alert('Your feedback has been submitted successfully');
                        $scope.new_feedback = {username:authenticationSvc.getUserInfo().username}
                    }

                }, function(error) {
                    $scope.errormsg = error;
                });
        }

        $scope.feedback_config = {};
        $scope.load_feedback_config = function(){
            if(mainSvc.configs) {
                for (var i = 0; i < mainSvc.configs.length; i++) {
                    if (mainSvc.configs[i].module == "feedback") {
                        $scope.feedback_config = mainSvc.configs[i].configuration;
                        $scope.feedback_labels = mainSvc.configs[i].labels;
                        $scope.feedback_urls = mainSvc.configs[i].urls;
                    }
                }
                for (var i = 0; i < $scope.feedback_labels.length; i++){
                    if($scope.feedback_labels[i].component == 'feedback'){
                        $scope.page_title = $scope.feedback_labels[i].component_label
                        $scope.page_labels = $scope.feedback_labels[i].labels
                    }
                }
            }
        }

        $scope.load_feedback_config();
    }
])
/**
 * Created by chirag on 03/05/16.
 */
app.factory("mainSvc", function($http, $q, $window,$location,$rootScope) {
    var self = this;
    self.api_rooturl = "http://104.155.215.243:8098"
    self.configs = []
    self.forms = []
    self.changelocation = function(path){
        $location.path(path);
    }
    $rootScope.changelocation = function(path){
        self.changelocation(path);
    }
    return self;
})
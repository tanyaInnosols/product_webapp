/**
 * Created by chirag on 13/05/16.
 */
app.factory("attendanceSvc", function($http, $q, $window,$location,$rootScope,$filter,mainSvc,authenticationSvc) {
    var self = this;
    self.is_checked_in = false;
    self.need_checkout = false;
    self.load_attendance = function(){
        var date = $filter('date')(new Date(), 'yyyy-MM-dd')
        $http.get(mainSvc.api_rooturl + "/api/attendance/get_attendance/"+date+"/?username=" + authenticationSvc.getUserInfo().username )
            .then(function (result) {
                self.is_checked_in = false;
                if(result.data.isError){
                    //$scope.errormsg = result.data.msg;
                }
                else {
                    if(result.data.data.check_in){
                        self.is_checked_in = true;
                        if(result.data.data.attendance=='P') {
                            if (result.data.data.check_out == null || result.data.data.check_out == undefined) {
                                self.need_checkout = true;
                            }
                        }
                    }
                }
            }, function(error) {
                self.errormsg = error;
            });
    }
    return self;
})
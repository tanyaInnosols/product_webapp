/**
 * Created by chirag on 03/05/16.
 */

app.factory("authenticationSvc", ['$http', '$q', '$window','mainSvc','$rootScope','$cookies',
    function($http, $q, $window,mainSvc,$rootScope,$cookies) {

    var self = this;
    //var userInfo;

    function getUserInfo() {
        return self.userInfo;
    }

    function login(userName, password, rememberMe,callback) {
        var deferred = $q.defer();

        $http.get(mainSvc.api_rooturl + '/api/login/?source=app&username='+userName+'&password='+password
        ).then(function(result) {
                //console.log(result)
                if(result.data.status == 'success') {
                    self.userInfo = result.data.data;
                    //console.log(self.userInfo)
                    if (rememberMe) {
                        $window.sessionStorage["userInfo-smartinfiledapp"] = JSON.stringify(self.userInfo);
                    }
                    $rootScope.isauthenticated = true;
                    deferred.resolve(self.userInfo);
                    self.updateUserInfo(callback);
                }
                else{
                    deferred.reject({
                        error_obj:result.data.err,
                        error_msg:result.data.msg
                    });
                    if (callback) {
                        callback(result.data.msg);
                    }
                    //alert(result.data.msg)
                }
        }, function(error) {
            deferred.reject(error);
        });

        return deferred.promise;
    }

    function logout(callback) {
        var deferred = $q.defer();
        $window.sessionStorage["userInfo-smartinfiledapp"] = null;
        $rootScope.isauthenticated = false;
        self.userInfo = null;
        deferred.resolve({ authenticated: false });

        if(callback){
            callback();
        }

        //$http({
        //    method: "POST",
        //    url: logoutUrl,
        //    headers: {
        //        "access_token": userInfo.accessToken
        //    }
        //}).then(function(result) {
        //    $window.sessionStorage["userInfo"] = null;
        //    $rootScope.isauthenticated = false;
        //    userInfo = null;
        //    deferred.resolve(result);
        //}, function(error) {
        //    deferred.reject(error);
        //});

        return deferred.promise;
    }

    self.updateUserInfo = function(callback){
        var userinfo = self.userInfo
        $http.get(mainSvc.api_rooturl + "/api/config/get_config/?username=" + userinfo.username)
            .then(function (config_result) {
                mainSvc.configs = config_result.data.data;
                $rootScope.configs = config_result.data.data;
                $window.sessionStorage["config-" + self.userInfo.username  + "-smartinfiledapp" ] = JSON.stringify(config_result.data.data);
                $http.get(mainSvc.api_rooturl + "/api/forms/getallforms/?username=" + userinfo.username)
                    .then(function (form_result) {
                        mainSvc.forms = form_result.data.data;
                        $rootScope.forms = form_result.data.data;
                        $window.sessionStorage["forms-" + self.userInfo.username  + "-smartinfiledapp" ] = JSON.stringify(form_result.data.data);
                        if (callback) {
                            callback();
                        }
                    });
            });
    }

    function init() {
        //if ($window.sessionStorage["userInfo"]) {
        if($window.sessionStorage["userInfo-smartinfiledapp"]){
            //self.userInfo = JSON.parse($window.sessionStorage["userInfo"]);
            self.userInfo = JSON.parse($window.sessionStorage["userInfo-smartinfiledapp"]);
            if(self.userInfo!=null && self.userInfo!="null") {
                $rootScope.isauthenticated = true;

                mainSvc.forms = JSON.parse($window.sessionStorage["forms-" + self.userInfo.username + "-smartinfiledapp"]);
                $rootScope.forms = JSON.parse($window.sessionStorage["forms-" + self.userInfo.username + "-smartinfiledapp"]);

                mainSvc.configs = JSON.parse($window.sessionStorage["config-" + self.userInfo.username + "-smartinfiledapp"]);
                $rootScope.configs = JSON.parse($window.sessionStorage["config-" + self.userInfo.username + "-smartinfiledapp"]);
            }
        }
    }

    init();

    return {
        login: login,
        logout: logout,
        getUserInfo: getUserInfo,
        updateUserInfo: self.updateUserInfo
    };
}]);
// 
// Here is how to define your module 
// has dependent on mobile-angular-ui
// 
var app = angular.module('product_webapp', [
  'ngRoute',
  'mobile-angular-ui',
  'ngCookies',
  // touch/drag feature: this is from 'mobile-angular-ui.gestures.js'
  // it is at a very beginning stage, so please be careful if you like to use
  // in production. This is intended to provide a flexible, integrated and and 
  // easy to use alternative to other 3rd party libs like hammer.js, with the
  // final pourpose to integrate gestures into default ui interactions like 
  // opening sidebars, turning switches on/off ..
  'mobile-angular-ui.gestures',
  'angular.filter'
  //'ngMaterial'
]);

app.run(["$rootScope", "$location","$transform", function($rootScope, $location,$transform) {
  window.$transform = $transform;
  $rootScope.isauthenticated = false;

    $rootScope.$on('$routeChangeStart', function(){
      //alert('start');
        $rootScope.loading = true;
    });

  $rootScope.$on("$routeChangeSuccess", function(userInfo) {
    //alert('stop');
    $rootScope.loading = false;
    console.log(userInfo);
  });

  $rootScope.$on("$routeChangeError", function(event, current, previous, eventObj) {
    if (eventObj.authenticated === false) {
      $location.path("/login");
    }
  });
}]);

// 
// You can configure ngRoute as always, but to take advantage of SharedState location
// feature (i.e. close sidebar on backbutton) you should setup 'reloadOnSearch: false' 
// in order to avoid unwanted routing.
// 
app.config(function($routeProvider) {
  var resolve_method = {
    auth: ["$q", "authenticationSvc", function($q, authenticationSvc) {
      var userInfo = authenticationSvc.getUserInfo();

      if (userInfo) {
        return $q.when(userInfo);
      } else {
        return $q.reject({ authenticated: false });
      }
    }]
  }

  $routeProvider.when('/',              {templateUrl: 'views/home.html', reloadOnSearch: false, resolve: resolve_method});
  $routeProvider.when('/mark_attendance',        {templateUrl: 'views/mark_attendance.html', reloadOnSearch: false, resolve: resolve_method});
  $routeProvider.when('/view_attendance',        {templateUrl: 'views/view_attendance.html', reloadOnSearch: false, resolve: resolve_method});
  $routeProvider.when('/feedback',          {templateUrl: 'views/feedback.html', reloadOnSearch: false, resolve: resolve_method});
  $routeProvider.when('/checkout',     {templateUrl: 'views/checkout.html', reloadOnSearch: false, resolve: resolve_method});
  $routeProvider.when('/profile',       {templateUrl: 'views/profile.html', reloadOnSearch: false, resolve: resolve_method});
  $routeProvider.when('/settings',         {templateUrl: 'views/settings.html', reloadOnSearch: false, resolve: resolve_method});
  $routeProvider.when('/daywise',         {templateUrl: 'views/daywise.html', reloadOnSearch: false, resolve: resolve_method});
  $routeProvider.when('/defaultwo',         {templateUrl: 'views/defaultwo.html', reloadOnSearch: false, resolve: resolve_method});
  $routeProvider.when('/changepwd',         {templateUrl: 'views/changepwd.html', reloadOnSearch: false, resolve: resolve_method});
  $routeProvider.when('/dynamic_form',         {templateUrl: 'views/dynamic_form.html', reloadOnSearch: true, resolve: resolve_method});
  $routeProvider.when('/coming_soon',         {templateUrl: 'views/coming_soon.html', reloadOnSearch: true, resolve: resolve_method});
  $routeProvider.when('/login',      {templateUrl: 'views/login.html', reloadOnSearch: false});
});

#!/bin/bash
# Script for Moving Code to Production

######## CONSTANTS #########
workflow="default"
dest="chirag@104.155.215.243"
destDir="/home/chirag/www/product_webapp"
######## FUNCTIONS #########

function build
{
    gulp build
}

function setDest
{
    case $workflow in
        prod )
            ;;
        * )
            echo "Setting Default Server (Staging) ..."
            dest="chirag@104.155.215.243"
            destDir="/home/chirag/www/product_webapp"
    esac
}

function copybase
{
    echo "Copying Base Files ..."
    scp -r favicon.png $dest:$destDir/
    scp -r index.html $dest:$destDir/
    scp -r sidebar.html $dest:$destDir/
    scp -r views/* $dest:$destDir/views/
    scp -r screens/* $dest:$destDir/screens/
    scp -r js/* $dest:$destDir/js/
    scp -r img/* $dest:$destDir/img/
    scp -r images/* $dest:$destDir/images/
    scp -r fonts/* $dest:$destDir/fonts/
    scp -r css/* $dest:$destDir/css/
}

function help
{
    echo -e "--copy \t\t Copy Base Files"
    echo -e "--all \t\t Build & Copy All"
}

while [ "$1" != "" ]; do
    case $1 in
        --workflow | -w )
            workflow=$2
            setDest
            shift
            ;;
        -h | --help )
            help
            ;;
        --copy )
            setDest
            copybase
            ;;
        --all )
            setDest
            # build
            copybase
            ;;
        * )
            echo "Wrong Input. Try -h or --usage for help"
    esac
    shift
done

exit 0
